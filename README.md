# Sentence finder

Finds sentences from books hosted on the [gutenberg](http://www.gutenberg.org/) project.

The algorithm is based on the simple criteria that sentences start with a punctuation mark and a capital letter, and
end with another punctuation mark. It does not account for abbreviations (written with a dot) followed by a capital 
word, so these are also erroneously considered to be sentences. Sentences that contain commas or apostrophes are
skipped.

Sentences are grouped by length (number of words) in the database.


Requires an english dictionary file. On unix systems you can install the `wamerican` package with 
```
sudo apt install wamerican
```
to install an english dict in `/usr/share/dict/words`. This program assumes such a dict file exists.