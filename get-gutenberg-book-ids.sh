#!/bin/bash
for i in `seq 10000 99999`
do
	status_code=`curl -sI -o /dev/null -w "%{http_code}" http://www.gutenberg.org/files/"$i"/"$i"-0.txt`
	if [ "${status_code}" = "200" ]; then
		echo "$i" >> gutenberg-ids
	fi
	if [ $(( $i % 10000 )) -eq 0 ]; then
		echo Done with "$i" checks
	fi
done
