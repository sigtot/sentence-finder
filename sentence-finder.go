package main

import (
	"github.com/coreos/bbolt"
	"fmt"
	"time"
	"strconv"
	"os"
	"bufio"
	"io"
	"net/http"
	"io/ioutil"
	"regexp"
	"strings"
	"errors"
)

const minLength = 2
const maxLength = 15
const maxLetters = 50

const queryTimeout = 1

const numWorkers = 5

const dictPath = "/usr/share/dict/words"

var db *bolt.DB

func skipLines(reader *bufio.Reader, line int) {
	for l := 0; l < line; l++ {
		buf, _, err := reader.ReadLine()
		s := string(buf)
		fmt.Printf("Skipping line %s\n", s)
		if err != nil {
			if err == io.EOF {
				break
			}
			fmt.Errorf("Could not skip line at line number %d: %s\n", l, err)
		}
	}
}

// Returns true if fileName contains a line identical to str
// Returns false on non-nil error
func fileContains(str, fileName string) (bool, error) {
	f, err := os.Open(fileName)
	defer f.Close()
	if err != nil {
		fmt.Errorf("Could not open file %s: %s\n", fileName, err)
	}

	reader := bufio.NewReader(f)

	for {
		buf, _, err := reader.ReadLine()
		if err != nil {
			if err == io.EOF {
				break
			} else {
				return false, errors.New("Could not read line")
			}
		}
		if string(buf) == str {
			return true, nil
		}
	}
	return false, nil
}

func processBook(bookId int) {
	fmt.Printf("Starting download for bookid %d...\n", bookId)
	bookUrl := fmt.Sprintf("http://www.gutenberg.org/files/%d/%d-0.txt", bookId, bookId)

	defer incrementLinesRead()

	// Download the book
	resp, err := http.Get(bookUrl)
	if err != nil {
		fmt.Errorf("Could not download book: %s\n", err)
		return // Return before closing resp.Body as it is nil on error and can't be closed
	}
	defer resp.Body.Close()

	// Check if response OK
	if resp.StatusCode != http.StatusOK {
		fmt.Errorf("Status code %d not 200.\n", resp.StatusCode)
		return
	}

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Errorf("Could not read body: %s\n", err)
		return
	}
	bodyString := string(bodyBytes)
	resp.Body.Close() // Close the body immediately after we have converted it ot string to minimize memory usage
	fmt.Printf("Finished downloading book %d. Starting processing...\n", bookId)

	s := ""
	punctuationCount, sentenceCount, englishSentenceCount := 0, 0, 0
	for _, rune := range bodyString {
		// String is too long
		if len(s) >= maxLetters {
			s = ""
			punctuationCount = 0
		}

		// Make sure the string starts and ends with punctuation before we test it
		if string(rune) == "." || string(rune) == "!" || string(rune) == "?"  {
			punctuationCount++
			if punctuationCount == 1 {
				s = ""
			}
		}

		// Disregard any line breaks
		if string(rune) == "\n" || string(rune) == "\r" || string(rune) == "\n\r" {
			s += " "
			continue // Skip whitespace
		}

		s += string(rune)

		// Check if string is a sentence candidate
		if punctuationCount == 2 {
			// Check if sentence matches our wanted lengths
			func() {
				for l := minLength; l <= maxLength; l++ {
					regexString := fmt.Sprintf("^([.?!] [A-Z])[a-z]*( [a-z]+){%d}[.?!]$", l-1)
					r := regexp.MustCompile(regexString)
					if r.FindStringSubmatchIndex(s) != nil {
						s = strings.Trim(s, ".?!")
						s = strings.TrimLeft(s, " ") // Remove leading space
						words := strings.Split(s, " ")

						if len(words) != l {
							fmt.Printf("Split string produced a slice of length %d and not %d\n", len(words), l)
							return // This should never happen. Discard the sentence if it somehow does
						}

						// Sentence found
						sentenceCount++

						// Check if all words are english
						for i := 0; i < len(words); i++ {
							isEnglish, err := fileContains(words[i], dictPath)
							if err != nil {
								fmt.Printf("Could not check if word is english: %s\n", err)
								return // Discard sentence
							}
							if !isEnglish {
								return // Discard sentence if not english
							}
						}

						// Sentence is all english
						englishSentenceCount++

						// Add sentence to database
						err = db.Update(func(tx *bolt.Tx) error {
							b := tx.Bucket([]byte(strconv.Itoa(l)))
							id, err := b.NextSequence()
							if err != nil {
								return err
							}
							err = b.Put([]byte(strconv.Itoa(int(id))), []byte(s))
							if err != nil {
								return err
							}

							err = b.Put([]byte("last_id"), []byte(strconv.Itoa(int(id))))
							return nil
						})
						if err != nil {
							fmt.Printf("Could not add sentence to database ")
							return // Discard the sentence
						}
						return
					}
				}
			}()
			// Reset string and punctuation count
			s = ""
			punctuationCount = 0
		}

		// Stop processing if the book is not english
		if sentenceCount > 100 && englishSentenceCount < 10 {
			fmt.Printf("Book is probably french or something. Skipping.\n")
			return // Discard book
		}
	}

	fmt.Printf("Finished processing book with id %d\n", bookId)
}

func incrementLinesRead() {
	db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("meta"))
		v := b.Get([]byte("lines_read"))
		if v != nil {
			vStr, err := strconv.Atoi(string(v))
			if err != nil {
				fmt.Errorf("Could not convert lines read to string: %s\n", err)
			}
			err = b.Put([]byte("lines_read"), []byte(strconv.Itoa(int(vStr) + 1)))
			if err != nil {
				fmt.Errorf("Could not increment lines read: %s\n", err)
			}
		}
		return nil
	})
}

func main() {
	var err error
	db, err = bolt.Open("sentences.db", 0600, &bolt.Options{Timeout: queryTimeout * time.Second})

	if err != nil {
		fmt.Errorf("Could not open database: %s\n", err)
	}

	// Create buckets for all sentence lengths
	for l := minLength; l <= maxLength; l++ {
		db.Update(func(tx *bolt.Tx) error {
			_, err := tx.CreateBucketIfNotExists([]byte(strconv.Itoa(l)))
			if err != nil {
				return fmt.Errorf("Could not create bucket: %s\n", err)
			}
			return nil
		})
	}

	// Create bucket for lines read
	db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte("meta"))
		if err != nil {
			return fmt.Errorf("Could not create bucket: %s", err)
		}
		return nil
	})

	// Initialize lines read to 0 if it is not yet set
	db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("meta"))

		v := b.Get([]byte("lines_read"))
		if v == nil {
			err := b.Put([]byte("lines_read"), []byte(strconv.Itoa(0)))
			if err != nil {
				fmt.Errorf("Could not initialize lines_read to 0: %s\n", err)
			}
		}
		return nil
	})

	// Open the file
	f, err := os.Open("gutenberg-ids")
	if err != nil {
		errorStr := fmt.Sprintf("Could not open file %v\n", err)
		panic(errorStr)
	}
	defer f.Close()

	// Create buffered reader
	reader := bufio.NewReader(f)

	// Skip to current position
	linesRead := 0
	db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("meta"))
		v := b.Get([]byte("lines_read"))
		if v != nil {
			vStr, err := strconv.Atoi(string(v))
			if err != nil {
				errorStr := fmt.Sprintf("Could not convert lines read to string: %v\n", err)
				panic(errorStr)
			}
			linesRead = int(vStr)
		} else {
			linesRead = 0
		}
		return nil
	})
	skipLines(reader, linesRead)

	guard := make(chan struct{}, numWorkers)

	for {
		buf, _, err := reader.ReadLine()
		if err != nil {
			if err == io.EOF {
				break
			}
			fmt.Errorf("Could not read line: %s\n", err)
		}

		guard <- struct{}{} // will block if guard channel is already filled

		bufStr, err :=  strconv.Atoi(string(buf))

		if err != nil {
			errorStr := fmt.Sprintf("Could not convert lines read to string: %v\n", err)
			panic(errorStr)
		}

		go func(bookId int) {
			processBook(bookId)
			<-guard
		}(int(bufStr))
	}

	fmt.Println("Finished processing all books")
}
